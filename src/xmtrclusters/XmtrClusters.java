package xmtrclusters;

import ilog.concert.IloException;
import java.util.Collection;
import solvers.GreedyConstruction;
import solvers.MIPModel;
import solvers.MIPModel2;
import solvers.PairSwap;

/**
 * XmtrClusters experiments with solutions to a problem of clustering
 * transmitters and users.
 *
 * Problem source: https://or.stackexchange.com/questions/7471/how-to-perform-
 * clustering-of-two-different-sets-of-entities/
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class XmtrClusters {

  /**
   * Dummy constructor.
   */
  private XmtrClusters() { }

  /**
   * Runs the experiments.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create a problem instance.
    int nT = 20;   // # of transmitters
    int nU = 60;   // # of users
    int maxC = 7;  // maximum number of clusters
    int maxS = 5;  // maximum cluster size
    Problem prob = new Problem(nT, nU, maxC, maxS, 1234);

    // Try the greedy heuristic.
    GreedyConstruction greedy = new GreedyConstruction(prob);
    Collection<Collection<Integer>> g = greedy.getSolution();
    if (g.isEmpty()) {
      System.out.println("\nThe greedy heuristic failed.");
    } else {
      System.out.println("Greedy heuristic results:\n" + prob.report(g));
    }
    System.out.println("");

    // Try the swapping heuristic.
    long tl = 10000;  // run time limit (ms.)
    PairSwap swap = new PairSwap(prob, g, tl);
    Collection<Collection<Integer>> s = swap.getSolution();
    System.out.println("Pairwise swapping results:\n" + prob.report(s));
    System.out.println("");

    // Create and solve a MIP model.
    double timeLimit = 300;  // time limit in seconds for CPLEX
    try {
      MIPModel mip = new MIPModel(prob);
      // Add the heuristic solution as a starting solution.
      mip.hotStart(s);
      double obj = mip.solve(timeLimit);
      System.out.println("\nFinal objective value = " + obj);
      // Get and summarize the optimal transmitter clusters.
      Collection<Collection<Integer>> clusters = mip.getClusters();
      System.out.println("\n" + prob.report(clusters));
    } catch (IloException ex) {
      System.out.println("\nCPLEX blew up:\n" + ex.getMessage());
    }

    // Try the revised MIP model.
//    try {
//      MIPModel2 mip = new MIPModel2(prob);
//      double obj = mip.solve(timeLimit);
//      System.out.println("\nFinal objective value = " + obj);
//      // Get and summarize the optimal transmitter clusters.
//      Collection<Collection<Integer>> clusters = mip.getClusters();
//      System.out.println("\n" + prob.report(clusters));
//    } catch (IloException ex) {
//      System.out.println("\nCPLEX blew up:\n" + ex.getMessage());
//    }
  }

}
