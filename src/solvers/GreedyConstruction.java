package solvers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import xmtrclusters.Problem;

/**
 * GreedyConstruction implements a greedy construction heuristic for the
 * clustering problem.
 *
 * Initially, each transmitter is a cluster unto itself. In each iteration,
 * all legal merges of a pair of clusters are tested, and the one with maximum
 * benefit is implemented. If no merges are beneficial and the number of
 * clusters exceeds the maximum allowed, the least harmful legal merger is done.
 * This repeats until the solution is legal and cannot be improved by a merger,
 * or the solution violates the cluster limit and no pair of clusters can
 * be merged.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */

record Merger(int first, int second, double change) { }

public final class GreedyConstruction {
  private final List<Collection<Integer>> clusters;  // list of clusters
  private final Problem problem;                     // problem to solve
  private final int maxSize;                         // maximum cluster size

  /**
   * Constructs the heuristic instance and solves it.
   * @param prob the problem to solve
   */
  public GreedyConstruction(final Problem prob) {
    long time = System.currentTimeMillis();
    problem = prob;
    maxSize = prob.getMaxSize();           // max cluster size
    int maxCount = prob.getMaxClusters();  // max cluster count
    // Initialize the cluster list with singleton clusters.
    clusters = new ArrayList<>();
    for (int t = 0; t < prob.getNTrans(); t++) {
      HashSet<Integer> c = new HashSet<>();
      c.add(t);
      clusters.add(c);
    }
    // Loop indefinitely until no further changes can be made.
    while (true) {
      // Find the pair of clusters that can be merged with maximum benefit.
      Merger merger = findMerger();
      // If no merger is possible due to size limits, end the loop.
      if (merger == null) {
        break;
      }
      // If a beneficial merger was found, make it and loop.
      if (merger.change() > 0) {
        execute(merger);
        continue;
      }
      // If we fall to here, no beneficial merge is possible. Check compliance
      // with the cluster count limit.
      if (clusters.size() <= maxCount) {
        // The solution is in compliance, so exit.
        break;
      } else {
        // If a non-beneficial merger was found, do it and continue looping.
        if (merger == null) {
          // Out of compliance with no possible merger.
          clusters.clear();
          break;
        } else {
          execute(merger);
        }
      }
    }
    time = System.currentTimeMillis() - time;
    System.out.println("\nThe greedy construction heuristic ran for "
                       + time + " ms.");
  }

  /**
   * Finds the best feasible merger given the current list of clusters.
   * @return a merger record showing the indices of two clusters to merge and
   * the change in objective value, or null if there are no legal mergers
   */
  private Merger findMerger() {
    int first = -1;
    int second = -1;
    double benefit = Double.MIN_VALUE;
    for (int i = 0; i < clusters.size(); i++) {
      Collection<Integer> c1 = clusters.get(i);
      double obj1 = problem.clusterQuality(c1);
      for (int j = i + 1; j < clusters.size(); j++) {
        Collection<Integer> c2 = clusters.get(j);
        // Make sure the size limit is respected.
        if (c1.size() + c2.size() <= maxSize) {
          // Compute the benefit of a merge.
          Collection<Integer> z = new HashSet<>(c1);
          z.addAll(c2);
          double delta =
            problem.clusterQuality(z) - obj1 - problem.clusterQuality(c2);
          if (delta > benefit) {
            benefit = delta;
            first = i;
            second = j;
          }
        }
      }
    }
    // Return the merger (or null if none was found).
    if (first < 0) {
      return null;
    } else {
      return new Merger(first, second, benefit);
    }
  }

  /**
   * Implements a merger.
   * @param merger the merger to implement
   */
  private void execute(final Merger merger) {
    HashSet<Integer> merged = new HashSet<>(clusters.get(merger.first()));
    merged.addAll(clusters.get(merger.second()));
    clusters.remove(merger.second());
    clusters.remove(merger.first());
    clusters.add(merged);
  }

  /**
   * Gets the final result of the heuristic.
   * @return the collection of clusters found
   */
  public Collection<Collection<Integer>> getSolution() {
    return Collections.unmodifiableCollection(clusters);
  }
}
